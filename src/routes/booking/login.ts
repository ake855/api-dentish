import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { UsersModel } from '../../models/booking/users';
import { ProfilesModel } from '../../models/booking/profiles';
import { RolesModel } from '../../models/booking/roles';
import { HospitalsModel } from '../../models/booking/hospitals';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const usersModel = new UsersModel();
  const profilesModel = new ProfilesModel();
  const rolesModel = new RolesModel();
  const hospitalsModel = new HospitalsModel();

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }

    try {
        let user: any;
        let profile: any;
        let role: any;
        let hospital: any;

        let users :any = await usersModel.doLogin(db,data.username,data.password);
        if(users[0]){
            user = users[0];
            let profiles :any = await profilesModel.getByCID(db,user.user_id);
            if(profiles[0]){
                profile = profiles;
                let hospitals :any = await hospitalsModel.getByID(db,profile.hospital_id);
                if(hospitals[0]){
                    hospital=hospitals[0];
                }
            }
            let roles :any = await rolesModel.getByID(db,user.role_id);
            if(roles[0]){
                role = roles;
                delete role.is_active;
            }
            const date = new Date();
            const iat = Math.floor(date.getTime() / 1000);
            const exp = Math.floor((date.setDate(date.getDate() + 7)) / 1000);

            const info: any = {
            id: user.username,
            name: profile.fullname,
            hospcode: hospital.hospital_code,
            hospname: hospital.hospital_name,
            role: role,
            email: 'ubondev@gmail.com',
            avatar: 'assets/images/avatars/brian-hughes.jpg',
            status: 'online'
            }

            const payload: any = {
            iat: iat,
            iss: 'UbonDev',
            exp: exp
            }
            const token = fastify.jwt.sign(payload)

        return reply.status(StatusCodes.CREATED)
            .send({
            status:StatusCodes.CREATED,
            ok: true,
            results : {accessToken: token,info: info ,tokenType: 'bearer'}
            });
        }else {
            return reply.status(204)
            .send({
            status:204,
            ok: false,
            results : `Not Authorized Success`
            });
        }
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  done();
}