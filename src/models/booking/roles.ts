import { Knex } from 'knex';

export class RolesModel {

  list(db: Knex) {
    return db('roles');
  }  

  getByID(db: Knex, id: number) {
    return db('roles')
    .where('role_id', id)
    .andWhere('is_active');
  }  

  getSearch(db: Knex, text: string) {
    return db('roles')
    .whereLike('role_name', text)
    .andWhere('is_active');
  }  

  create(db: Knex, data: any) {
    return db('roles')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('roles')
    .where('role_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('roles')
      .where('role_id', id)
      .delete();
  }

}