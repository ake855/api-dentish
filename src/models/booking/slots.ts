import { Knex } from 'knex';

export class SlotsModel {

  list(db: Knex) {
    return db('slots');
  }  

  getByID(db: Knex, id: number) {
    return db('slots')
    .where('slot_id', id).andWhere('is_active');
  }  

  getByPeriodID(db: Knex, id: number) {
    return db('slots')
    .where('period_id', id)
    .andWhere('is_active');
  } 

  getByServiceID(db: Knex, id: number) {
    return db('slots')
    .where('service_id', id)
    .andWhere('is_active');
  }  

  getByServiceTypeID(db: Knex, id: number) {
    return db('slots')
    .where('service_type_id', id)
    .andWhere('is_active');
  }  

  getSearch(db: Knex, text: string) {
    return db('slots')
    .whereLike('slot_name', text);
  }  

  create(db: Knex, data: any) {
    return db('slots')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('slots')
    .where('slot_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('slots')
      .where('slot_id', id)
      .delete();
  }

}