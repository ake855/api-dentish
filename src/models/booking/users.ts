import { Knex } from 'knex';
import * as crypto from 'crypto';

export class UsersModel {

  list(db: Knex) {
    return db('users');
  }  

  getByID(db: Knex, id: number) {
    return db('users')
    .where('user_id', id)
    .andWhere('is_active');
  }  

  getByRoleID(db: Knex, id: number) {
    return db('users')
    .where('role_id', id)
    .andWhere('is_active');
  }  

  create(db: Knex, data: any) {
    return db('users')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('users')
    .where('user_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('users')
      .where('user_id', id)
      .delete();
  }

  doLogin(db: Knex, username: string, password: string) {
    const encPassword = crypto.createHash('md5').update(password).digest('hex');
    return db('users')
    .where('username', username)
    .andWhere('password', encPassword)
    .andWhere('is_active');
  }  

}